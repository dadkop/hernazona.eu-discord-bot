const ms = require("ms");
const config = require('../../config.json');

module.exports = {
    name: 'startgiveaway',
    aliases: ['giveaway'],
    category: 'giveaways',
    description: 'Spustí nový giveaway',
    usage: '[príkaz | alias] <#channel> <čas> <počet víťazov> <výhra>',
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('MANAGE_MESSAGES') && !message.member.roles.cache.some((r) => r.name === "Giveaways")){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        let giveawayChannel = message.mentions.channels.first();
        if(!giveawayChannel) return message.reply(':x: Musíš dať mention na channel.');

        let giveawayDuration = args[1];
        if(!giveawayDuration || isNaN(ms(giveawayDuration))){
            return message.reply(':x: Musíš špecifikovať čas!');
        }

        let giveawayNumberWinners = args[2];
        if(isNaN(giveawayNumberWinners) || (parseInt(giveawayNumberWinners) <= 0)){
            return message.reply(':x: Musíš špecifikovať počet víťazov!');
        }

        let giveawayPrize = args.slice(3).join(' ');
        if(!giveawayPrize){
            return message.reply(':x: Musíš špecifikovať výhru!');
        }

        client.giveawaysManager.start(giveawayChannel, {
            time: ms(giveawayDuration),
            prize: giveawayPrize,
            winnerCount: giveawayNumberWinners,
            hostedBy: config.hostedBy ? message.author : null,
            messages: {
                giveaway: (config.everyoneMention ? '@everyone\n\n' : '') + '🎉🎉 **GIVEAWAY** 🎉🎉',
                giveawayEnded: (config.everyoneMention ? '@everyone\n\n' : '') + '🎉🎉 **GIVEAWAY UKONČENÝ** 🎉🎉',
                timeRemaining: 'Čas do ukončenia: **{duration}**!',
                inviteToParticipate: 'Reaguje s 🎉 pre zapojenie!',
                winMessage: 'Gratulujeme, {winners}! Vyhral si **{prize}**!',
                embedFooter: 'Giveaway',
                noWinner: 'Giveaway zrušený, nikto sa nezapojil.',
                hostedBy: 'Súťaž vytvoril: {user}',
                winners: 'víťaz(i)',
                endedAt: 'Skončilo',
                units: {
                    seconds: 'sekúnd',
                    minutes: 'minút',
                    hours: 'hodín',
                    days: 'dní',
                    pluralS: false
                }
            }
        });

        message.channel.send(`Giveaway začal v ${giveawayChannel}!`);
    }
};