const ms = require("ms");

module.exports = {
    name: 'endgiveaway',
    aliases: ['egiveaway'],
    category: 'giveaways',
    description: 'Ukončí giveaway',
    usage: '[príkaz | alias] <ID | názov>',
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('MANAGE_MESSAGES') && !message.member.roles.cache.some((r) => r.name === "Giveaways")){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        if(!args[0]){
            return message.channel.send(':x: Musíš špecifikovať ID alebo názov!');
        }

        let giveaway = client.giveawaysManager.giveaways.find((g) => g.prize === args.join(' ')) || client.giveawaysManager.giveaways.find((g) => g.messageID === args[0]);
        if(!giveaway){
            return message.channel.send('Neviem nájsť tento giveaway: `'+ args.join(' ') +'`.');
        }

        client.giveawaysManager.edit(giveaway.messageID, {
            setEndTimestamp: Date.now()
        }).then(() => {
            message.channel.send('Giveaway skončí o ' + (client.giveawaysManager.options.updateCountdownEvery/1000) + ' sekúnd...');
        }).catch((e) => {
            if(e.startsWith(`Giveaway s ID ${giveaway.messageID} už skončil.`)){
                message.channel.send('Tenti giveaway už skončil!');
            } else {
                console.error(e);
                message.channel.send('Nastala chyba...');
            }
        });
    }
};