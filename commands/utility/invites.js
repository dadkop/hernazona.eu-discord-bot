const { replaceResultTransformer } = require("common-tags");

module.exports = {
    name: "invites",
    category: "utility",
    description: "Zobrazí leaderboard pozvaní",
    usage: "[príkaz | alias]",
    run: async (client, message, args) => {
        message.delete();
        
        const { guild } = message

        guild.fetchInvites().then((invites) => {
            const inviteCounter = {}

            invites.forEach((invite) => {
                const { uses, inviter } = invite

                const name = `<@${inviter.id}>`;

                inviteCounter[name] = (inviteCounter[name] || 0) + uses;
            })

            let replyText = '**Leaderboard Pozvaní**\n';

            const sortedInvites = Object.keys(inviteCounter).sort(
                (a, b) => inviteCounter[b] - inviteCounter[a]
            )

            console.log(sortedInvites);

            sortedInvites.length = 3;

            for (const invite of sortedInvites) {
                const count = inviteCounter[invite];
                let say = 'členov';
                if(count === 1) say = 'člena';
                replyText += `\n${invite} pozval ${count} ${say}!`;
            }

            message.channel.send(replyText);
        })
    }
};