const Discord = require('discord.js')

module.exports = {
    name: 'whois',
    aliases: ['info'],
    category: 'utility',
    description: 'Vypíše informácie o užívateľovi',
    usage: '[príkaz | alias] <user>',
    run: async (client, message, args) => {
        message.delete();
        {
            const user = message.mentions.users.first();
            
            if(!user) return message.reply('prosím označ užívateľa, ktorého si chceš pozrieť.');
            
            var playing = ('[ ' + user.presence.activities + ' ]')

            const kto = new Discord.MessageEmbed()
                .setTitle('Info')
                .addField('Nick', `${user.tag}`)
                .addField('ID', user.id)
                .addField('Playing', playing, true)
                .addField('Status', `${user.presence.status}`, true)
                .addField('Prišiel na Discord', user.createdAt.toLocaleDateString())
                .setColor('RANDOM')
                .setTimestamp()
                .setThumbnail(user.avatarURL())
            message.channel.send(kto);
        }
    }
};