const Discord = require("discord.js")
const { version } = require("discord.js");
const moment = require("moment");
const m = require("moment-duration-format");
let os = require('os');
let cpuStat = require("cpu-stat");
const ms = require("ms");

module.exports = {
    name: 'botinfo',
    aliases: ['binfo'],
    category: 'utility',
    description: 'Zobrazí detailné informácie o bot klientovi',
    usage: '[príkaz | alias]',
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('ADMINISTRATOR')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        let cpuLol;
        let totalMembers = 0;
        for(const guild of client.guilds.cache) {
            totalMembers += (await guild[1].members.fetch()).size;
        }
        cpuStat.usagePercent(function(err, percent) {
            if(err) return console.log(err);
            const duration = moment.duration(client.uptime);
            
            if(duration.days() == 1) {
                days = `${duration.days()} deň`;
            } else if(duration.days() > 1 && duration.days() < 5) {
                days = `${duration.days()} dni`;
            } else {
                days = `${duration.days()} dní`;
            }
            if(duration.hours() == 1) {
                hours = `${duration.hours()} hodinu`;
            } else if(duration.hours() > 1 && duration.hours() < 5) {
                hours = `${duration.hours()} hodiny`;
            } else {
                hours = `${duration.hours()} hodín`;
            }
            if(duration.minutes() == 1) {
                minutes = `${duration.minutes()} minútu`;
            } else if(duration.minutes() > 1 && duration.minutes() < 5) {
                minutes = `${duration.minutes()} minúty`;
            } else {
                minutes = `${duration.minutes()} minút`;
            }
            if(duration.seconds() == 1) {
                seconds = `${duration.seconds()} sekundu`;
            } else if(duration.seconds() > 1 && duration.seconds() < 5) {
                seconds = `${duration.seconds()} sekundy`;
            } else {
                seconds = `${duration.seconds()} sekúnd`;
            }
            const botinfo = new Discord.MessageEmbed()
                .setAuthor(client.user.username)
                .setTitle("__**Stats:**__")
                .setColor("RANDOM")
                .addField("⏳ RAM využitie:", `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} / ${(os.totalmem() / 1024 / 1024).toFixed(2)} MB`, true)
                .addField("⌚️ Uptime ", `${days}, ${hours}, ${minutes}, ${seconds}`, true)
                .addField("📁 Užívatelia", `${totalMembers}`, true)
                .addField("📁 Servery", `${client.guilds.cache.size}`, true)
                .addField("📁 Channels ", `${client.channels.cache.size}`, true)
                .addField("👾 Discord.js", `v${version}`, true)
                .addField("🤖 Node", `${process.version}`, true)
                .addField("🤖 CPU", `\`\`\`md\n${os.cpus().map(i => `${i.model}`)[0]}\`\`\``)
                .addField("🤖 CPU využitie", `\`${percent.toFixed(2)}%\``, true)
                .addField("🤖 Arch", `\`${os.arch()}\``, true)
                .addField("💻 Platforma", `\`\`${os.platform()}\`\``, true)
                .addField("🏓 API Ping", `${(client.ws.ping)}ms`)  
            message.channel.send(botinfo)
        });
    }
};