const Discord = require("discord.js");

module.exports = {
    name: "serverinfo",
    aliases: ['sinfo'],
    category: "utility",
    description: "Zobrazí info o tomto serveri",
    usage: "[príkaz | alias]",
    run: async (client, message, args) => {
        message.delete();
        let servericon = message.guild.iconURL;
        let dateFormat = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        dateFormat.timeZoneName = 'long';
        let serverembed = new Discord.MessageEmbed()
            .setTitle("Server Informácie")
            .setColor("RANDOM")
            .setThumbnail(servericon)
            .addField("Server Názov", message.guild.name)
            .addField("Majiteľ", `${message.guild.owner.user.username}#${message.guild.owner.user.discriminator}`, true)
            .addField("Channels", message.guild.channels.cache.size, true)
            .addField("Roles", message.guild.roles.cache.size, true)
            .addField("Vytvorený dňa", message.guild.createdAt.toLocaleDateString('sk-SK', dateFormat))
            .addField("Pridal si sa dňa", message.member.joinedAt.toLocaleDateString('sk-SK', dateFormat))
            .addField("Celkový počet členov", message.guild.memberCount)
            .setThumbnail(message.guild.iconURL())
            .setTimestamp()
            .setFooter(message.author.username, message.author.avatarURL());
        message.channel.send(serverembed);
    }
};