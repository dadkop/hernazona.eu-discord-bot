const Discord = require('discord.js')
const { MessageEmbed } = require('discord.js');

module.exports = {
    name: "remove",
    category: "tickets",
    description: "Odstráni používateľa do ticketu",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.channel.name.toLowerCase().startsWith('ticket-')) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Zakázané`, `Príkaz bol zrušený - nemáš povolenie použiť tento príkaz mimo svojho ticketu!`);
            return message.channel.send({embed: embed});
        }
        
        let member = message.mentions.users.first();
        if(!member || member.id === client.user.id) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Neplatné argumenty`, `**Použitie:** \`!remove @member\`\n**Príklad:** \`!remove @${message.author.tag}\``);
            return message.channel.send({embed: embed});
        }

        if(message.channel.topic === member.id) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Zakázané`, `Nemôžeš odstrániť autora ticketu, ak chceš odísť, požiadaj o to staff!`);
            return message.channel.send({embed: embed});
        }

        if(member.id == message.author.id) {
            let embed = new MessageEmbed().setColor('ORANGE').addField(`:warning: Upozornenie`, `Musíš použiť príkaz **!close**!`);
            return message.channel.send({embed: embed});
        }

        if(!message.channel.permissionsFor(member).has(['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY'])) {
            let embed = new MessageEmbed().setColor('ORANGE').addField(`:warning: Pozor`, `**${member.tag}** v tomto tickete nie je!`);
            return message.channel.send({embed: embed});
        }

        message.channel.updateOverwrite(member, { SEND_MESSAGES: false, VIEW_CHANNEL: false, READ_MESSAGE_HISTORY: false });
        let embed = new MessageEmbed().setColor('BLUE').addField(`:white_check_mark: Používateľ Odstránený!`, `Úspešne odstránený **${member.tag}** z ticketu!`).setFooter(`Odstránený používateľom: ${message.author.tag}`);
        message.channel.send({embed: embed});
    }
};