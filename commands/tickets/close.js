const Discord = require('discord.js')
const { MessageEmbed } = require('discord.js');

module.exports = {
    name: "close",
    category: "tickets",
    description: "Zatvorí ticket",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.channel.name.toLowerCase().startsWith('ticket-')) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Zakázané`, `Príkaz bol zrušený - nemáš povolenie použiť tento príkaz mimo svojho ticketu!`);
            return message.channel.send({embed: embed});
        }
        
        let reacting = await message.channel.send({embed: new MessageEmbed().setColor('ORANGE').setFooter(`${client.user.tag}`, client.user.displayAvatarURL()).addField(`Potvrdenie`, `Prosím reaguj :white_check_mark: aby si uzavrel tento ticket!`)});
        reacting.react('✅');

        let f = (reaction, user) => reaction.emoji.name === '✅' && user.id === message.author.id;

        let collector = reacting.createReactionCollector(f, { max: 1, time: 60000 });

        setTimeout(function() {
            if(message.guild.channels.cache.get(message.channel.id)) {
                reacting.delete();
            }
        }, 61000);

        collector.on('collect', async collected => {
            reacting.edit({embed: new MessageEmbed().setColor('GREEN').addField(`:white_check_mark: Úspech`, `Zatváram ticket...`)});
            setTimeout(function() { message.channel.delete(); }, 2000);
        });
    }
};