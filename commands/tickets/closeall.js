const Discord = require('discord.js')
const { MessageEmbed } = require('discord.js');
const config = require('../../config.json');

module.exports = {
    name: "closeall",
    category: "tickets",
    description: "Zatvorí všetky tickety",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();

        if(!message.member.hasPermission('ADMINISTRATOR')) {
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        let reacting = await message.channel.send({embed: new MessageEmbed().setColor('ORANGE').setTitle(':warning: Upozornenie').setDescription(`Tento príkaz sa nedá vrátiť späť! Prosím klikni na :white_check_mark: pre potvrdenie, že chceš uzavrieť všetky tickety!\nAk nechceš, igonruj reakciu!`).addField(`Potvrdenie`, `Prosím reaguj :white_check_mark: aby si uzavrel tento ticket!`)});
        reacting.react('✅');

        let f = (reaction, user) => reaction.emoji.name === '✅' && user.id === message.author.id;

        let collector = reacting.createReactionCollector(f, { max: 1, time: 60000 });

        setTimeout(function() {
            if(message.guild.channels.cache.get(message.channel.id)) {
                reacting.delete();
            }
        }, 61000);

        collector.on('collect', async collected => {
            reacting.reactions.removeAll();
            reacting.edit({embed: new MessageEmbed().setColor('GREEN').addField(`:white_check_mark: Úspech`, `Zatváram všetky tickety...`)});
            
            let category = message.guild.channels.cache.get(config.tickets.category);
            if(category) category.children.forEach(async channel => { await channel.delete(); });
            else message.guild.channels.filter(c => c.name.toLowerCase().startsWith('ticket')).forEach(async channel => { await channel.delete(); });

            message.channel.send({embed: new MessageEmbed().setColor('GREEN').setFooter(message.author.tag, client.user.displayAvatarURL()).setDescription('✅ Úspešne zatvorené všetky otvorené tickety!').setTimestamp()});
        });
    }
};