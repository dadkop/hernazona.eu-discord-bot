const Discord = require('discord.js')
const { MessageEmbed } = require('discord.js');

module.exports = {
    name: "add",
    category: "tickets",
    description: "Pridá používateľa do ticketu",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.channel.name.toLowerCase().startsWith('ticket-')) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Zakázané`, `Príkaz bol zrušený - nemáš povolenie použiť tento príkaz mimo svojho ticketu!`);
            return message.channel.send({embed: embed});
        }
        
        let member = message.mentions.users.first();
        if(!member) {
            let embed = new MessageEmbed().setColor('RED').addField(`:x: Neplatné argumenty`, `**Použitie:** \`!add @member\`\n**Príklad:** \`!add @${message.author.tag}\``);
            return message.channel.send({embed: embed});
        }

        if(message.channel.permissionsFor(member).has(['SEND_MESSAGES', 'VIEW_CHANNEL', 'READ_MESSAGE_HISTORY'])) {
            let embed = new MessageEmbed().setColor('ORANGE').addField(`:warning: Pozor`, `**${member.tag}** už v tomto tickete je!`);
            return message.channel.send({embed: embed});
        }

        message.channel.updateOverwrite(member, { SEND_MESSAGES: true, VIEW_CHANNEL: true, READ_MESSAGE_HISTORY: true });
        let embed = new MessageEmbed().setColor('BLUE').addField(`:white_check_mark: Používateľ Pridaný!`, `Úspešne pridaný **${member.tag}** do ticketu!`).setFooter(`Pridaný používateľom: ${message.author.tag}`);
        message.channel.send({embed: embed});
    }
};