const Discord = require('discord.js')
const { MessageEmbed } = require('discord.js');
const config = require('../../config.json');

module.exports = {
    name: "t-new",
    aliases: ['ticket'],
    category: "tickets",
    description: "Vytvorí nový ticket",
    usage: "[príkaz | alias]",
    run: async (client, message, args) => {
        var [parent, support, supervisor] = [config.tickets.category, message.guild.roles.cache.get(config.tickets.roles.support), message.guild.roles.cache.get(config.tickets.roles.supervisor)];
        let co = true;
        let arg = args.slice(0).join(' ');

        function check() {
            let channels = message.guild.channels.cache.filter(ch => ch.type === 'text' && ch.name.startsWith('ticket-'));
            channels.forEach(channel => {
                if(channel.topic.includes(message.author.id)) co = false;
            });
        }
        check();

        if(message.channel.id !== '784837775315697687') {
            message.react('❌');

            let errmsg = 'podať ticket môžeš len v roomke <#784837775315697687>';
            return message.reply(errmsg)
        }

        if(!arg) {
            message.delete();
            let embed = new MessageEmbed().addField(':x: Error', 'Napíš dôvod/nadpis tvojho ticketu!').setColor('RED');
            return message.channel.send({embed: embed});
        }

        message.delete();

        if(!co) {
            let embed = new MessageEmbed().addField(':x: Error', 'Už máš otvorený ticket. Prosím ukonči ho predtým ako vytvoríš ďalší!').setColor('RED');
            return message.channel.send({embed: embed});
        }

        let ticket = await message.guild.channels.create(`ticket-${message.author.discriminator}`, 'text');

        let ch = await message.guild.channels.cache.find(channel => channel.id === parent);
        if(ch && ch.type === 'category') ticket.setParent(ch.id);
        
        ticket.setTopic(message.author.id);

        ticket.overwritePermissions([
            {
                id: message.guild.roles.everyone.id,
                deny: ['VIEW_CHANNEL'],
            },
            {
                id: message.author.id,
                allow: ['VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'SEND_MESSAGES'],
            },
            {
                id: support.id,
                allow: ['VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'SEND_MESSAGES'],
            },

            {
                id: supervisor.id,
                allow: ['VIEW_CHANNEL', 'READ_MESSAGE_HISTORY', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
            },
        ]);
        let embed = new MessageEmbed().setColor('BLUE').setTitle(arg).setAuthor(`Vitaj ${message.author.username}`, message.author.displayAvatarURL()).setDescription(config.tickets.messages.welcome);
        ticket.send({embed: embed});

        embed = new MessageEmbed().setColor('GREEN').addField(`✅ Ticket Vytvorený`, `Úspešne vytvorený tvoj ticket <#${ticket.id}>`);
        message.channel.send({embed: embed});
    }
};