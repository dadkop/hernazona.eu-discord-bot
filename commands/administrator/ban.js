const Discord = require('discord.js')

module.exports = {
    name: "ban",
    category: "administrator",
    description: "Zabanuje používateľa zo serveru",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('BAN_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const member = message.mentions.users.first();
        if(member) {
            const memberTarget = message.guild.member.cache.get(member.id);
            memberTarget.ban();
            message.reply('užívateľ bol zabanovaný.');
        } else {
            message.reply('koho mám zabanovať?');
        }
    }
};