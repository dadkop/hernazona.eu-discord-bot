const { MessageEmbed } = require('discord.js');
const muteSchema = require('../../schemas/mute-schema');

module.exports = {
    name: "is-muted",
    aliases: ['muted'],
    category: "administrator",
    description: "Zobrazí, či je user mutnutý",
    usage: "[príkaz | alias] <user id | user @>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('KICK_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const { guild } = message;

        if (args.length !== 1) {
            message.reply(`správna syntax: !muted <User ID | user @>`);
            return;
        }

        const mentionUser = message.mentions.users.first();
        if(mentionUser && args[0].replace('!', '').match(mentionUser)) {
            id = mentionUser.id;
        } else {
            id = args[0];
        }

        const members = await guild.members.fetch();
        const target = members.get(id);
        const isInDiscord = !!target;

        const currentMute = await muteSchema.findOne({
            userId: id,
            guildId: guild.id,
            current: true,
        })

        const embed = new MessageEmbed()
            .setAuthor(`Mute info pre ${target ? target.user.tag : id}`, target ? target.user.displayAvatarURL() : '')
            .addField('Je mutnutý', currentMute ? 'Áno' : 'Nie')
            .addField('Je na tomto Discorde', isInDiscord ? 'Áno' : 'Nie')

        if (currentMute) {
            const date = new Date(currentMute.expires);

            embed
                .addField('Mutnutý od', `<@${currentMute.staffId}>`)
                .addField('Mutnutý za', currentMute.reason.toLowerCase())
                .addField('Mute vyprší', `${date.toLocaleString('sk-SK')} ET`)
        }

        message.channel.send(embed);
    }
};