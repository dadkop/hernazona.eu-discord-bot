const Discord = require('discord.js')
const muteSchema = require('../../schemas/mute-schema');

const reasons = {
    spam: 5,
    reklama: 24,
    troll: 336,
}

module.exports = {
    name: "mute",
    category: "administrator",
    description: "Umlčí používateľa",
    usage: "[príkaz | alias] <user> <dôvod>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('KICK_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const { guild, author: staff } = message;

        if (args.length !== 2) {
            message.reply(`Správna syntax: !mute <user @> <dôvod>`);
            return;
        }

        const target = message.mentions.users.first();
        if (!target) {
            message.reply('prosím zadaj komu chceš dať mute');
            return;
        }

        const reason = args[1].toLowerCase();
        if (!reasons[reason]) {
            let validReasons = '';
            for (const key in reasons) {
                validReasons += `${key}, `;
            }
            validReasons = validReasons.substr(0, validReasons.length - 2);

            message.reply(`neznámy dôvod, použi jednu z nasledujúcich možností: ${validReasons}`);
            return;
        }

        const previousMutes = await muteSchema.find({
            userId: target.id,
        });

        const currentlyMuted = previousMutes.filter((mute) => {
            return mute.current === true;
        });

        if (currentlyMuted.length) {
            message.reply('tento používateľ je už mutnutý.');
            return;
        }

        let duration = reasons[reason] * (previousMutes.length + 1);

        const expires = new Date();
        expires.setHours(expires.getHours() + duration);

        const mutedRole = guild.roles.cache.find((role) => {
            return role.name === 'muted';
        });
        if (!mutedRole) {
            message.reply('nemôžem nájsť "muted" rolu!');
            return;
        }

        const targetMember = (await guild.members.fetch()).get(target.id);
        targetMember.roles.add(mutedRole);

        await new muteSchema({
            userId: target.id,
            guildId: guild.id,
            reason,
            staffId: staff.id,
            staffTag: staff.tag,
            expires,
            current: true,
        }).save();

        message.reply(`mutol si <@${target.id}> za "${reason}". Unmute dostane za ${duration} hodín.`);
    }
};