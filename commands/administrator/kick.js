const Discord = require('discord.js')

module.exports = {
    name: "kick",
    category: "administrator",
    description: "Vyhodí používateľa zo serveru",
    usage: "[príkaz | alias] <user>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('KICK_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const member = message.mentions.users.first();
        if(member) {
            const memberTarget = message.guild.member.cache.get(member.id);
            memberTarget.kick();
            message.reply('užívateľ bol vyhodený.');
        } else {
            message.reply('koho mám vyhodiť?');
        }
    }
};