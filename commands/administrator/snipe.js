const { MessageEmbed } = require('discord.js');

module.exports = {
    name: "snipe",
    category: "administrator",
    description: "Zobrazí poslednú zmazanú správu",
    usage: "[príkaz | alias]",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('KICK_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const msg = client.snipes.get(message.channel.id);
        const embed = new MessageEmbed()
            .setColor('RED')
            .setAuthor(msg.author, msg.member.user.displayAvatarURL())
            .setDescription(msg.content)
            .setFooter(`Snipe od ${message.author.username}`, message.author.avatarURL())
            .setTimestamp()
        
        message.channel.send(embed);
    }
};