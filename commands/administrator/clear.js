const Discord = require('discord.js')

module.exports = {
    name: "clear",
    aliases: ['purge', 'cl', 'del'],
    category: "administrator",
    description: "Vymaže zadaný počet správ",
    usage: "[príkaz | alias] <hodnota>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('MANAGE_MESSAGES')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        if(!args[0]) return message.reply('zadaj počet správ, ktoré chceš vymazať.');
        if(isNaN(args[0])) return message.reply('zadaj číslo!');

        if(args[0] > 100) return message.reply('nemôžem vymazať viac než 100 správ naraz!');
        if(args[0] < 1) return message.reply('musíš vymazať aspoň 1 správu!');

        await message.channel.messages.fetch({ limit: args[0] }).then(messages => {
            message.channel.bulkDelete(messages);
        });
    }
};