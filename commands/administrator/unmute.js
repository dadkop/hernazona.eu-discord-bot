const Discord = require('discord.js');
const muteSchema = require('../../schemas/mute-schema');

module.exports = {
    name: "unmute",
    category: "administrator",
    description: "Zruší mute používateľa",
    usage: "[príkaz | alias] <user | ID>",
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('KICK_MEMBERS')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const { guild } = message;

        if (args.length !== 1) {
            message.reply(`použi správnu syntax: !mute <user @ alebo jeho ID>`);
            return;
        }

        let id = '';

        const target = message.mentions.users.first();
        if (target) {
            id = target.id;
        } else {
            id = args[0];
        }

        const result = await muteSchema.updateOne(
            {
                guildId: guild.id,
                userId: id,
                current: true,
            },
            {
                current: false,
            }
        )

        if (result.nModified === 1) {
            const mutedRole = guild.roles.cache.find((role) => {
            return role.name === 'muted';
            })

            if (mutedRole) {
                const guildMember = guild.members.cache.get(id);
                guildMember.roles.remove(mutedRole);
            }

            message.reply(`umnutol si <@${id}>`);
        } else {
            message.reply('tento užívateľ nie je mutnutý!');
        }
    }
};