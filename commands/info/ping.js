module.exports = {
    name: 'ping',
    aliases: ['latency'],
    category: 'info',
    description: 'Vráti latenciu a API ping',
    usage: '[príkaz | alias]',
    run: async (client, message, args) => {
        message.delete();
        const msg = await message.channel.send('🏓 Pinging...');
        msg.edit(`🏓 Pong! | Ping: ${msg.createdTimestamp - message.createdTimestamp}ms. API Ping ${Math.round(client.ws.ping)}ms`)
    }
};