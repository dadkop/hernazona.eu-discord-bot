const { MessageEmbed } = require('discord.js');
const { stripIndents } = require('common-tags');
const utils = require('../../utils.js');

module.exports = {
    name: 'help',
    aliases: ['h'],
    category: 'info',
    description: 'Vráti všetky príkazy alebo jeden špicifický príkaz',
    usage: '[príkaz | alias]',
    run: async (client, message, args) => {
        message.delete();
        if(args[0]) {
            return getCMD(client, message, args[0]);
        } else {
            return getAll(client, message);
        }
    }
};

function getAll(client, message) {
    const embed = new MessageEmbed()
        .setColor('#ffdfd3')
        .setTitle('Help Menu')
        .setFooter('Ak chceš zobraziť popis príkazov a typ, použi !help [názov príkazu]')

        const commands = (category) => {
            return client.commands
                .filter(cmd => cmd.category === category)
                .map(cmd => `\`${cmd.name}\``)
                .join(", ");
        }

        const info = client.categories
            .map(cat => stripIndents`**${cat[0].toUpperCase() + cat.slice(1)}** \n${commands(cat)}`)
            .reduce((string, category) => string + "\n" + category);

            message.reply('príkazy som ti poslal do dm.')

        return message.author.send(embed.setDescription(info));
}

function getCMD(client, message, input) {
    const embed = new MessageEmbed()
    const cmd = client.commands.get(input.toLowerCase() || client.commands.get(client.aliases.get(input.toLowerCase())));
    let info = `Nenašiel som žiadne informácie pre príkaz **${input.toLowerCase()}**`;

    if(!cmd) {
        return message.channel.send(embed.setColor('RED').setDescription(info));
    }

    if(cmd.name) info = `**Príkaz**: ${cmd.name}`;
    if(cmd.aliases) info += `\n**Aliasy**: ${cmd.aliases.map(a => `\`${a}\``).join(", ")}`;
    if(cmd.description) info += `\n**Popis**: ${cmd.description}`;
    if(cmd.usage) {
        info += `\n**Použitie**: ${cmd.usage}`;
        embed.setFooter(`Syntax: <> = požadované, [] = voliteľné`);
    }

    return message.channel.send(embed.setColor('GREEN').setDescription(info));
}