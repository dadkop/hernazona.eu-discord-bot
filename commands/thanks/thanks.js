const thanksSchema = require('../../schemas/thanks-schema');
const updateLeaderboard = require('../../features/features/thanks-leaderboard');

module.exports = {
    name: 'thanks',
    aliases: ['thx'],
    category: 'thanks',
    description: 'Poďakuje niekomu',
    usage: '[príkaz | alias] <user>',
    run: async (client, message, args) => {
        message.delete();
        const target = message.mentions.users.first();
        if(!target) {
            message.reply('prosím označ niekoho, komu chceš poďakovať!');
            return;
        }

        const { guild } = message;
        const guildId = guild.id;
        const targetId = target.id;
        const authorId = message.author.id;
        const now = new Date();

        if(targetId === authorId) {
            message.reply('nemôžeš poďakovať sám sebe!');
            return;
        }

        const authorData = await thanksSchema.findOne({
            userId: authorId,
            guildId,
        });

        if(authorData && authorData.lastGave) {
            const then = new Date(authorData.lastGave);

            const diff = now.getTime() - then.getTime();
            const diffHours = Math.round(diff / (1000 * 60 * 60));

            const hours = 12;
            if (diffHours <= hours) {
                message.reply(`už si niekomu poďakoval v posledných ${hours} hodinách.`);
            return;
            }
        }

        await thanksSchema.findOneAndUpdate(
            {
                userId: authorId,
                guildId,
            }, {
                userId: authorId,
                guildId,
                lastGave: now,
            }, {
                upsert: true,
            }
        );

        const result = await thanksSchema.findOneAndUpdate(
            {
                userId: targetId,
                guildId,
            }, {
                userId: targetId,
                guildId,
                $inc: {
                    received: 1,
                },
            }, {
                upsert: true,
                new: true,
            }
        );

        const amount = result.received;
        if(amount == 1) {
            say = 'poďakovanie';
        } else if(amount > 1 && amount < 5) {
            say = 'poďakovania';
        } else {
            say = 'poďakovaní';
        }
        message.reply(`poďakoval si užívateľovi <@${targetId}>! Má teraz **${amount}** ${say}.`);
        
        setTimeout(() => {
            updateLeaderboard(client);
        }, 5000);
    }
};