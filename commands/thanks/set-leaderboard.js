const thanksLeaderboardSchema = require('../../schemas/thanks-leaderboard-schema');

module.exports = {
    name: 'set-leaderboard',
    aliases: ['sl'],
    category: 'thanks',
    description: 'Nastaví leaderboard',
    usage: '[príkaz | alias] <user>',
    run: async (client, message, args) => {
        message.delete();
        if(!message.member.hasPermission('ADMINISTRATOR')){
            return message.reply(':x: Na toto nemáš dostatočné práva');
        }

        const { guild, channel } = message;
        const guildId = guild.id;
        const channelId = channel.id;

        await thanksLeaderboardSchema.findOneAndUpdate(
            {
                _id: guildId,
                channelId,
            }, {
                _id: guildId,
                channelId,
            }, {
                upsert: true,
            }
        );

        message.reply('leaderboard set!').then((message) => {
            message.delete({
                timeout: 1000 * 5,
            });
        });
    }
};