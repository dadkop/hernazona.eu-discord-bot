const { Client, Collection } = require('discord.js');
const fs = require('fs');
const util = require('minecraft-server-util');

const config = require('./config.json');
const loadFeatures = require('./features/load-features');

const mongo = require('./mongo');

const client = new Client({
    disableEveryone: true,
});

client.commands = new Collection();
client.aliases = new Collection();
client.snipes = new Collection();

client.categories = fs.readdirSync('./commands/');

//commands handler
["commands"].forEach(handler => {
    require(`./handlers/${handler}`)(client);
});

//messages
client.on('message', async message => {
    const prefix = (config.prefix);
    if(!message.content.startsWith(prefix) || message.author.bot) return;
    if(!message.member) message.member = await message.guild.fetchMember(message);
    if(!message.guild) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/);
    const cmd = args.shift().toLowerCase();

    if(cmd.length === 0) return;

    let command = client.commands.get(cmd);
    if(!command) command = client.commands.get(client.aliases.get(cmd));

    if(command) command.run(client, message, args);
});

//giveaways
const { GiveawaysManager } = require('discord-giveaways');
client.giveawaysManager = new GiveawaysManager(client, {
    storage: './giveaways.json',
    updateCountdownEvery: 5000,
    default: {
        botsCanWin: false,
        embedColor: '#ff0000',
        reaction: '🎉'
    }
});

client.giveawaysManager.on('giveawayReactionAdded', (giveaway, member, reaction) => {
    console.log(`${member.user.tag} reagoval na giveaway #${giveaway.messageID} (${reaction.emoji.name})`);
});

client.giveawaysManager.on('giveawayReactionRemoved', (giveaway, member, reaction) => {
    console.log(`${member.user.tag} odreagoval na giveaway #${giveaway.messageID} (${reaction.emoji.name})`);
});

//players counter
function updatePlayers() {
    util.query('mc.hernazona.eu', { port: 25583 }).then(response => {
        client.channels.cache.get('806879151679340546').setName(`Hráči na serveroch: ${response.onlinePlayers}`);
    });
}

//boot
client.on('ready', () => {
    mongo();
    loadFeatures(client);
    setInterval(updatePlayers, 1000*60*5);
    client.user.setStatus('online');
    client.user.setActivity('HernáZóna.eu | !help', {type: 'PLAYING'});
    console.log('Bot booted!');
    console.log('Welcome to [ HernáZóna.eu Bot ] made by Polkov.');
});

//login
client.login(config.token);