module.exports = client => {
    const channelId = '784410411600445471';

    const updateMembers = guild => {
        const channel = guild.channels.cache.get(channelId);
        channel.setName(`Je nás tu: ${guild.memberCount.toLocaleString()}`);
    }

    client.on('guildMemberAdd', (member) => updateMembers(member.guild));
    client.on('guildMemberRemove', (member) => updateMembers(member.guild));

    const guild = client.guilds.cache.get('771338277126668319');
    updateMembers(guild);
}