module.exports = (client) => {
    const invites = {}
  
    const getInviteCounts = async (guild) => {
        return await new Promise((resolve) => {
            guild.fetchInvites().then((invites) => {
                const inviteCounter = {}
  
                invites.forEach((invite) => {
                    const { uses, inviter } = invite;
  
                    const name = `<@${inviter.id}>`;
  
                    inviteCounter[name] = (inviteCounter[name] || 0) + uses;
                });
  
                resolve(inviteCounter);
            });
        });
    }
  
    client.guilds.cache.forEach(async (guild) => {
        invites[guild.id] = await getInviteCounts(guild);
    });
  
    client.on('guildMemberAdd', async (member) => {
        const { guild, id } = member;
  
        const invitesBefore = invites[guild.id];
        const invitesAfter = await getInviteCounts(guild);
  
        console.log('BEFORE:', invitesBefore);
        console.log('AFTER:', invitesAfter);
  
        for (const inviter in invitesAfter) {
            if (invitesBefore[inviter] === invitesAfter[inviter] - 1) {
                const channelId = '771367213746880542';
                const channel = guild.channels.cache.get(channelId);
                const count = invitesAfter[inviter];
                channel.send(`Privítajte **<@${id}>** na tomto Discorde! <@${id}>, čítaj prosím <#771798698488496149> a pridaj si svoje role v <#792049650580324372>!`);
  
                invites[guild.id] = invitesAfter;
                return;
            }
        }
    });
}