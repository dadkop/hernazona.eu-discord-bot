const muteSchema = require('../../schemas/mute-schema');

module.exports = (client) => {
    const isInvite = async (guild, code) => {
        return await new Promise(resolve => {
            guild.fetchInvites().then((invites) => {
                for(const invite of invites) {
                    if(code === invite[0]) {
                        resolve(true);
                        return;
                    }
                }

                resolve(false);
            });
        });
    }

    client.on('message', async (message) => {
        const { guild, member, content } = message;
        const code = content.split('discord.gg/')[1];

        const wl = ['191243099123482625', '237646139719942144', '646077748607057920', '317663389922885633', '349195182538883082'];

        if(content.includes('discord.gg/')) {
            const isOurInvite = await isInvite(guild, code);

            if(!isOurInvite) {
                if(wl.includes(message.author.id)) return;

                message.delete();
                
                let muteRole = message.guild.roles.cache.find(role => role.name === 'muted');
                member.roles.add(muteRole.id);

                const previousMutes = await muteSchema.find({
                    userId: member.id,
                });

                let duration = 24 * (previousMutes.length + 1);

                const expires = new Date();
                expires.setHours(expires.getHours() + duration);

                const botName = message.client.user;

                await new muteSchema({
                    userId: member.id,
                    guildId: guild.id,
                    reason: 'reklama',
                    staffId: botName.id,
                    staffTag: botName.tag,
                    expires,
                    current: true,
                }).save();

                message.reply(`neposielaj invite link na iný discord! Dostávaš mute na ${duration} hodín!`);
            }
        }
    });

    client.on('messageUpdate', async (oldMessage, newMessage) => {
        const { guild, member, content } = newMessage;
        const code = content.split('discord.gg/')[1];

        const wl = ['191243099123482625', '237646139719942144', '646077748607057920', '317663389922885633', '349195182538883082'];

        if(newMessage.content.includes('discord.gg/')) {
            const isOurInvite = await isInvite(guild, code);
            if(!isOurInvite) {
                if(wl.includes(newMessage.author.id)) return;

                newMessage.delete();
                
                let muteRole = newMessage.guild.roles.cache.find(role => role.name === 'muted');
                member.roles.add(muteRole.id);

                const previousMutes = await muteSchema.find({
                    userId: member.id,
                });

                let duration = 24 * (previousMutes.length + 1);

                const expires = new Date();
                expires.setHours(expires.getHours() + duration);

                const botName = newMessage.client.user;

                await new muteSchema({
                    userId: member.id,
                    guildId: guild.id,
                    reason: 'reklama',
                    staffId: botName.id,
                    staffTag: botName.tag,
                    expires,
                    current: true,
                }).save();

                newMessage.reply(`neposielaj invite link na iný discord! Dostávaš mute na ${duration} hodín!`);
            }
        }
    });
}