const { MessageEmbed } = require('discord.js');
const firstMessage = require('../../utils');

module.exports = (client) => {
    const channelId = '792049650580324372';

    const getEmoji = (emojiName) =>
        client.emojis.cache.find((emoji) => emoji.name === emojiName);

    const emojis = {
        minecraft: 'Minecraft',
        csgo: 'Counter Strike: Global Offensive',
        rl: 'Rocket League',
        LoL: 'League of Legends',
        among: 'Among Us',
        child: '12 rokov a menej',
        kid: '13-15 rokov',
        sexypepe: '16-19 rokov',
        gun: '20-24 rokov',
        boss: '25 rokov a viac',
        girl: 'Žena',
        boy: 'Muž',
        peeposlovak: 'Slovakia',
        peepoczech: 'Czechia',
        underages: 'NSFW',
    }

    const reactions = [];
    
    let emojiText = '>>> **Vyber si adekvátne role**\n\n';

    for (const key in emojis) {
        const emoji = getEmoji(key);
        reactions.push(emoji);

        const role = emojis[key];
        emojiText += `${emoji} = ${role}\n`;
    }

    firstMessage(client, channelId, emojiText, reactions);

    const handleReaction = (reaction, user, add) => {
        if (user.id === '783711890911526922') {
            return;
        }

        const emoji = reaction._emoji.name;

        const { guild } = reaction.message;

        const roleName = emojis[emoji];
        if (!roleName) {
            return;
        }

        const role = guild.roles.cache.find((role) => role.name === roleName);
        const member = guild.members.cache.find((member) => member.id === user.id);

        if (add) {
            member.roles.add(role);
        } else {
            member.roles.remove(role);
        }
    }

    client.on('messageReactionAdd', (reaction, user) => {
        if (reaction.message.channel.id === channelId) {
            handleReaction(reaction, user, true);
        }
    });

    client.on('messageReactionRemove', (reaction, user) => {
        if (reaction.message.channel.id === channelId) {
            handleReaction(reaction, user, false);
        }
    });
}