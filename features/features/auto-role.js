module.exports = (client) => {
    client.on('guildMemberAdd', member => {
        const rola = member.guild.roles.cache.find(rola => rola.name === "Member");
        member.roles.add(rola);
    })
}